/**
 * 
 */
package caf.war.NewTask.tasktestoverview;

/**
 * @author adam.kiss
 *
 */

import com.webmethods.caf.faces.data.task.TaskDisplayProvider;

public class TaskTestOverviewDefaultviewView extends com.webmethods.caf.faces.bean.BasePageBean {


	private static final long serialVersionUID = 1L;
	private static final String[][] INITIALIZE_PROPERTY_BINDINGS = new String[][] {
		{"#{activePreferencesBean.currentTab}", "TaskData"},
	};

	// binding the Task Display Provider to the current taskID (passed to the Portlet Bean via the URL)
	private TaskDisplayProvider taskDisplayProvider = null;
	private static final String[][] TASKDISPLAYPROVIDER_PROPERTY_BINDINGS = new String[][] { {
			"#{TaskDisplayProvider.taskID}", "#{TaskTestOverview.taskID}" }, };
	private transient caf.war.NewTask.tasktestoverview.TaskTestOverview taskTestOverview = null;
	private caf.war.NewTask.taskclient.TaskTest taskTest = null;
	private static final String[][] TASKTEST_PROPERTY_BINDINGS = new String[][] {
		{"#{TaskTest.taskID}", "#{TaskTestOverview.taskID}"},
	};

	/**
	 * Initialize page
	 */
	public String initialize() {
		try {
		    resolveDataBinding(INITIALIZE_PROPERTY_BINDINGS, null, "initialize", true, false);
		} catch (Exception e) {
			error(e);
			log(e);
		}
		return null;	
	}

	/*
	 * Get the Task Display Provider for the current taskID
	 */
	public TaskDisplayProvider getTaskDisplayProvider() {
		if (taskDisplayProvider == null) {
			taskDisplayProvider = (TaskDisplayProvider) resolveExpression("#{TaskDisplayProvider}");
		}
		resolveDataBinding(TASKDISPLAYPROVIDER_PROPERTY_BINDINGS,
				taskDisplayProvider, "taskDisplayProvider", false, false);
		return taskDisplayProvider;
	}

	public caf.war.NewTask.tasktestoverview.TaskTestOverview getTaskTestOverview()  {
		if (taskTestOverview == null) {
		    taskTestOverview = (caf.war.NewTask.tasktestoverview.TaskTestOverview)resolveExpression("#{TaskTestOverview}");
		}
		return taskTestOverview;
	}

	public caf.war.NewTask.taskclient.TaskTest getTaskTest()  {
		if (taskTest == null) {
		    taskTest = (caf.war.NewTask.taskclient.TaskTest)resolveExpression("#{TaskTest}");
		}
	
	    resolveDataBinding(TASKTEST_PROPERTY_BINDINGS, taskTest, "taskTest", false, false);
		return taskTest;
	}

}