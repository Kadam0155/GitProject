package caf.war.NewTask.taskclient;

 

import com.webmethods.caf.faces.data.task.ITaskData;
import com.webmethods.caf.faces.data.ContentProviderException;
 
/**
 * Task Client bean for 'TaskTest' task.
 */ 
public class TaskTest extends com.webmethods.caf.faces.data.task.impl.TaskContentProviderExtended {

	private static final long serialVersionUID = 7595013900259145728L;
	
	/**
	 * Globally unique task type id
	 */
	private static final String TASK_TYPE_ID = "E102E5BD-6387-C094-E131-665A7B391A0B";

	/**
	 * Default constructor
	 */
	public TaskTest() {
		super();
		setTaskTypeID(TASK_TYPE_ID);
	}
	
	/**
	 * Task Data Object
	 */
	public static class TaskData extends com.webmethods.caf.faces.data.task.impl.TaskData {

		private static final long serialVersionUID = 1230279757927471104L;
		 
			
		public static final String[] INPUTS = new String[] {};


		public static final String[] OUTPUTS = new String[] {};
 
		public TaskData() {
		}
		
		
		
	}
	
	/**
	 * Return current task data object
	 * @return current task data
	 */
	public TaskData getTaskData() {
		return (TaskData)getValue(PROPERTY_KEY_TASKDATA);
	}

	/**
	 * Creates new task data object
	 * @return newly created task data object
	 */	
	protected ITaskData newTaskData() throws ContentProviderException {
		return new TaskData();
	}

}