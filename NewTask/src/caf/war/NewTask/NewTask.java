/**
 * 
 */
package caf.war.NewTask;

/**
 * @author adam.kiss
 *
 */
public class NewTask extends com.webmethods.caf.faces.bean.BaseApplicationBean 
{
	public NewTask()
	{
		super();
		setCategoryName( "CafApplication" );
		setSubCategoryName( "NewTask" );
	}
}